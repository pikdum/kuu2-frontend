function makeSnowflake() {
	var snowflake = document.createElement('div');
	snowflake.innerHTML = '&#10052;';
	snowflake.style.position = 'fixed';
	snowflake.style.fontFamily = 'monospace';
	snowflake.style.fontSize = (Math.random() * 300 + 200) + '%';
	snowflake.style.color = localStorage.getItem('SNOWFLAKE_COLOR') || 'blue';
	snowflake.style.opacity = localStorage.getItem('SNOWFLAKE_OPACITY') || '0.25';
	snowflake.style.pointerEvents = 'none';
	snowflake.style.left = (Math.random() * 100) + 'vw';
	snowflake.style.top = '-10vh';
	snowflake.speed = Math.pow(Math.random(), 2) + 0.3;
	return snowflake;
}

function moveSnowflake(snowflake) {
	if (document.visibilityState !== "visible") {
		return setTimeout(function() { moveSnowflake(snowflake) }, 1000);
	}
	var x = snowflake.style.left;
	var y = snowflake.style.top;
	var s = snowflake.speed;
	var c = 2;
	x = x.replace('vw', '');
	y = y.replace('vh', '');
	if (y > 100 || x < -10) {
		snowflake.style.left = (Math.random() * 100) + 'vw';
		snowflake.style.top = '-10vh';
		snowflake.speed = Math.pow(Math.random(), 2) + 0.3;
		snowflake.style.fontSize = (Math.random() * 300 + 200) + '%';
		return moveSnowflake(snowflake);
	}
	x = (parseFloat(x) - Math.random()) + 'vw';
	y = (parseFloat(y) + (s * c)) + 'vh';

	$(snowflake).animate({ 'left': x, 'top': y }, {
		duration: 150,
		complete: function() {
			moveSnowflake(snowflake);
		}
	});
	return true;
}

function snowstorm(count) {
	if (count <= 0) {
		return;
	}
	var snowflake = makeSnowflake();
	document.body.appendChild(snowflake);
	moveSnowflake(snowflake);
	setTimeout(function() { snowstorm(count - 1); }, Math.random() * 10000);
}
